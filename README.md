In this project, we will classify benign and malignant melanomas using images.<br>
We will first clean the dataset (and use an external dataset containing more malignant cases).<br>
Then we will make predictions with efficientnet
